const fs        = require('fs');
const moment    = require('moment');
const puppeteer = require('puppeteer');
const sqlite3   = require('sqlite3');

const maxLogPages = 50;

const {
    url,
    password,
    db: dbPath,
    ignore = []
} = JSON.parse(fs.readFileSync('config.json'));

const ignoreRx = ignore.map(i => new RegExp(i));

const db = new sqlite3.Database(dbPath);

function createTable() {
    return new Promise((resolve, reject) => {
        db.run(`
            CREATE TABLE logs (
                ctime INTEGER NOT NULL,
                msg   TEXT    NOT NULL,
                CONSTRAINT u UNIQUE (ctime, msg)
            )
        `, err => err ? reject(err) : resolve());
    });
}

(async () => {

    // Create logs table if it doesn't exist
    try {
        await createTable();
    } catch(err) {
        if (!err.toString().match(/table logs already exists/)) {
            console.error(`Failed to create logs table in sqlite db:\n\n${err.toString()}`);
            return;
        }
    }

    const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]
    });

    const page = await browser.newPage();

    await page.goto(url);
    await titleWanted(page, 'Plusnet Hub Manager - Home');

    await Promise.all([
        page.waitForNavigation(),
        page.$eval('img[alt=troubleshooting]', el => el.parentNode.click()),
    ]);
    await titleWanted(page, 'Plusnet Hub Manager - Help and Advice');

    await Promise.all([
        page.waitForNavigation(),
        page.$$eval('a', links => links.filter(link => link.textContent === 'Event Log')[0].click()),
    ]);
    await titleWanted(page, 'Plusnet Hub Manager - Login');

    await page.type('#password', password);
    await Promise.all([
        page.waitForNavigation(),
        page.click('a[href="SendPassword()"]'),
    ]);
    await titleWanted(page, 'Plusnet Hub Manager - Event Log');

    const insertLog = db.prepare("INSERT INTO logs VALUES (?, ?)");
    try {
        for(let n = 0; n < maxLogPages; ++n) {

            const logs = await page.evaluate(() => {
                const logs = [];
                for (const row of document.querySelectorAll('#frame_div > table:nth-of-type(2) tr[bgcolor]')) {
                    const date  = row.querySelector('td:nth-child(1)').textContent;
                    const entry = row.querySelector('td:nth-child(2)').textContent;
                    logs.push({ date, entry });
                }
                return logs;
            });

            for (const { date, entry } of logs) {
                if (ignoreRx.some(rx => entry.match(rx))) continue;
                await new Promise(resolve => insertLog.run(parseDate(date).unix(), entry, resolve));
            }

            await Promise.all([
                page.waitForNavigation(),
                page.click('a[name=submit_button_bt_next]')
            ]);
        }
    } catch(err) {
        if (!err.toString().match(/No node found for selector/)) {
            console.error(err.toString());
            return;
        }
    }

    await browser.close();

})();

async function titleWanted(page, wanted) {
    const title = await page.evaluate(() => document.title);
    if (title !== wanted) throw new Error(`Bad document title. Wanted "${wanted}", received "${title}"`);
}

function parseDate(str) {
    let [ _, hour, min, sec, mday, mon ] = str.match(/^(\d\d):(\d\d):(\d\d).*?(\d+).*?([A-Z][a-z]{2})/);
    mon = { Jan: '01', Feb: '02', Mar: '03', Apr: '04', May: '05', Jun: '06', Jul: '07', Aug: '08', Sep: '09', Oct: '10', Nov: '11', Dec: '12'}[mon];

    const year = moment().format('YYYY');
    const when = moment(`${year}-${mon}-${mday} ${hour}:${min}:${sec}`);
    
    // Deal with year roll-overs. Assumes we don't see logs more than a yearish old
    const nextWeek = moment().add(1, 'week');
    if (nextWeek.isBefore(when)) when.subtract(1, 'year');

    return when;
}
