# Plusnet Hub One modem/router logs collector

* Author - Mike Cardwell - https://www.grepular.com/
* License - GPL v3 - Please see COPYING.txt
* Copyright (&copy;) Mike Cardwell

## Description

This application logs into your Plusnet Hub One modem/router's web based admin UI, reads the logs from the troubleshooting pages and inserts them into an sqlite database. You can run it repeatedly and it will only insert new logs. The Plusnet Hub One modem/router truncates some logs at 24 hours, so if you want to keep them longer, this app will help you.

It's a nodejs app, and I use bleeding edge versions of node for async goodness, so if you want to run it, you'll need to make sure you're up to date. It uses headless Chrome to access the web UI. I originally tried to make something a little bit more light weight without a full browser, but the UI uses Javascript in various annoying places and this was the quickest solution for me.

## Usage

You need to create a `config.json` file in the root of the repository. It should look like something like this:

```json
{
    "url": "https://192.168.101.254",
    "password": "$YOUR_MODEM_PASSWORD",
    "db": "./plusnet_logs.sqlite",
    "ignore": [
        "Connection (closed|opened) \\(Port Forwarding: ",
        "Admin login successful by",
        "New GUI session from IP"
    ]
}
```

- `url` is the URL for accessing your modem. Yours is probably different
- `password` is the admin user password for your modems web ui
- `db` is the path to the sqlite db. One will be created in it's place if it doesn't exist
- `ignore` is an optional array of strings containing regular expressions for log entries to ignore

Run it from a cron job:

```cron
30 * * * * cd /path/to/plusnet_logs && npm start > /dev/null
```